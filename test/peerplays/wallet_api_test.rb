require 'test_helper'

module Peerplays
  class WalletApiTest < Peerplays::Test
    def setup
      if TEST_NODE == Peerplays::ChainConfig::NETWORKS_PEERPLAYS_DEFAULT_NODE
        skip 'not enabled (cannot test this api with a public node)'
      end
      
      @api = Peerplays::WalletApi.new(url: TEST_NODE, check_enabled: true) rescue skip('not enabled')
    end
    
    def test_method_missing
      assert_raises NoMethodError do
        @api.bogus
      end
    end
    
    def test_glist_my_accounts_empty
      assert_raises Peerplays::ArgumentError do
        @api.list_my_accounts()
      end
    end
  end
end
