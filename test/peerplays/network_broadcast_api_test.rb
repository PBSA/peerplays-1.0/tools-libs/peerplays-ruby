require 'test_helper'

module Peerplays
  class NetworkBroadcastApiTest < Peerplays::Test
    def setup
      @api = Peerplays::NetworkBroadcastApi.new(url: TEST_NODE, check_enabled: true) rescue skip('not enabled')
    end
    
    def test_method_missing
      assert_raises NoMethodError do
        @api.bogus
      end
    end
    
    def test_broadcast_transaction_empty
      assert_raises Peerplays::ArgumentError do
        @api.broadcast_transaction()
      end
    end
    
    def test_broadcast_transaction_with_callback_empty
      assert_raises Peerplays::ArgumentError do
        @api.broadcast_transaction_with_callback()
      end
    end
    
    def test_broadcast_block_empty
      assert_raises Peerplays::ArgumentError do
        @api.broadcast_block()
      end
    end
    
    def test_broadcast_block_too_old
      options = {
        block: {
          previous: "0000000000000000000000000000000000000000",
          timestamp: "1970-01-01T00:00:00",
          witness: "",
          transaction_merkle_root: "0000000000000000000000000000000000000000",
          extensions: [],
          witness_signature: "0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
          transactions: []
        }
      }
      
      assert_raises Peerplays::BlockTooOldError do
        @api.broadcast_block(options)
      end
    end
    
    def test_broadcast_transaction
      options = {
        trx: {
          ref_block_num: 0,
          ref_block_prefix: 0,
          expiration: "1970-01-01T00:00:00",
          operations: [],
          extensions: [],
          signatures: []
        },
        max_block_age: -1
      }
      
      assert_raises EmptyTransactionError do
        @api.broadcast_transaction(options)
      end
    end
  end
end
