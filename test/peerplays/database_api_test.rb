require 'test_helper'

module Peerplays
  class DatabaseApiTest < Peerplays::Test
    def setup
      @api = Peerplays::DatabaseApi.new(url: TEST_NODE, check_enabled: true) rescue skip('not enabled')
    end
    
    def test_method_missing
      assert_raises NoMethodError do
        @api.bogus
      end
    end
    
    def test_get_objects_empty
      assert_raises Peerplays::ArgumentError do
        @api.get_objects()
      end
    end
    
    def test_get_objects_none
      options = [
        [], # object_id_type array ids
        [] # boolean array subscribe
      ]
      
      @api.get_objects(*options) do |result|
        assert_equal Hashie::Array, result.class
        assert_equal 0, result.size
      end
    end
    
    def test_get_objects
      options = [
        ['1.11.6722984'], # object_id_type array ids
        [false] # boolean array subscribe
      ]
      
      @api.get_objects(*options) do |result|
        assert_equal Hashie::Array, result.class
        assert_equal 1, result.size
      end
    end
    
    def test_set_subscribe_callback_empty
      assert_raises Peerplays::ArgumentError do
        @api.set_subscribe_callback()
      end
    end
    
    def test_set_subscribe_callback
      options = [
        99, # variant cb
        true # boolean notify_remove_create
      ]
      
      @api.set_subscribe_callback(*options) do |result|
        assert_nil result
      end
    end
    
    def test_set_pending_transaction_callback_empty
      assert_raises Peerplays::ArgumentError do
        @api.set_pending_transaction_callback()
      end
    end
    
    def test_set_pending_transaction_callback
      options = [
        99 # signed_transaction_object cb
      ]
      
      @api.set_pending_transaction_callback(*options) do |result|
        assert_nil result
      end
    end
    
    def test_set_block_applied_callback_empty
      assert_raises Peerplays::ArgumentError do
        @api.set_block_applied_callback()
      end
    end
    
    def test_set_block_applied_callback
      options = [
        99 # block_id cb
      ]
      
      @api.set_block_applied_callback(*options) do |result|
        assert_nil result
      end
    end
    
    def test_cancel_all_subscriptions
      assert_nil @api.cancel_all_subscriptions()
    end
    
    def test_get_block_header_empty
      assert_raises Peerplays::ArgumentError do
        @api.get_block_header()
      end
    end
    
    def test_get_block_header
      options = [
        1 # uint32_t block_num
      ]
      
      @api.get_block_header(*options) do |result|
        assert_equal Hashie::Mash, result.class
        assert_equal 7, result.size, "expect 7 keys, got: #{result.keys}"
      end
    end
    
    def test_get_block_empty
      assert_raises Peerplays::ArgumentError do
        @api.get_block()
      end
    end
    
    def test_get_block
      options = [
        1 # uint32_t block_num
      ]
      
      @api.get_block(*options) do |result|
        assert_equal Hashie::Mash, result.class
        assert_equal 9, result.size, "expect 9 keys, got: #{result.keys}"
      end
    end
    
    def test_get_transaction_empty
      assert_raises Peerplays::ArgumentError do
        @api.get_transaction()
      end
    end
    
    def test_get_transaction
      options = [
        31125932, # uint32_t block_num
        0, # uint32_t trx_in_block
      ]
      
      @api.get_transaction(*options) do |result|
        assert_equal Hashie::Mash, result.class
        assert_equal 7, result.size, "expect 7 keys, got: #{result.keys}"
      end
    end
    
    def test_get_chain_properties
      @api.get_chain_properties do |result|
        assert_equal Hashie::Mash, result.class
        assert_equal 3, result.size, "expect 3 keys, got: #{result.keys}"
      end
    end
    
    def test_get_global_properties
      @api.get_global_properties do |result|
        assert_equal Hashie::Mash, result.class
        assert_equal 5, result.size, "expect 5 keys, got: #{result.keys}"
      end
    end
    
    def test_get_config
      @api.get_config do |result|
        assert_equal Hashie::Mash, result.class
        assert_equal 76, result.size, "expect 76 keys, got: #{result.keys}"
      end
    end
    
    def test_get_chain_id
      @api.get_chain_id do |result|
        assert_equal String, result.class
        assert_equal ChainConfig::NETWORKS_PEERPLAYS_CHAIN_ID, result, 'Unexpected chain id'
      end
    end
    
    def test_get_dynamic_global_properties
      @api.get_dynamic_global_properties do |result|
        assert_equal Hashie::Mash, result.class
        assert_equal 14, result.size, "expect 14 keys, got: #{result.keys}"
      end
    end
    
    def test_get_key_references_empty
      assert_raises Peerplays::ArgumentError do
        @api.get_key_references()
      end
    end
    
    def test_get_key_references_none
      options = [
        [], # public_key_type array keys
      ]
      
      @api.get_key_references(*options) do |result|
        assert_equal Hashie::Array, result.class
        assert_equal 0, result.size
      end
    end
    
    def test_get_key_references_invalid
      options = [
        ['1234'], # public_key_type array keys
      ]
      
      assert_raises Peerplays::BaseError do
        @api.get_key_references(*options)
      end
    end
    
    def test_get_key_references_empty_pub
      options = [
        ['PPY1111111111111111111111111111111114T1Anm'], # public_key_type array keys
      ]
      
      assert_raises Peerplays::BaseError do
        @api.get_key_references(*options)
      end
    end
    
    def test_get_key_references
      options = [
        ['PPY8GC13uCZbP44HzMLV6zPZGwVQ8Nt4Kji8PapsPiNq1BK153XTX'], # public_key_type array keys
      ]
      
      @api.get_key_references(*options) do |result|
        assert_equal Hashie::Array, result.class
        assert_equal 0, result.flatten.size
      end
    end
    
    def test_get_accounts_empty
      assert_raises Peerplays::ArgumentError do
        @api.get_accounts()
      end
    end
    
    def test_get_accounts_none
      options = [
        [], # string array account_names_or_ids
        false, # boolean array subscribe
      ]
      
      @api.get_accounts(*options) do |result|
        assert_equal Hashie::Array, result.class
        assert_equal 0, result.size
      end
    end
    
    def test_get_accounts_invalid
      options = [
        ['1234'], # string array account_names_or_ids
        false, # boolean array subscribe
      ]
      
      assert_raises Peerplays::BaseError do
        @api.get_accounts(*options)
      end
    end
    
    def test_get_accounts
      options = [
        ['1.2.8900'], # string array account_names_or_ids
        false, # boolean array subscribe
      ]
      
      @api.get_accounts(*options) do |result|
        assert_equal Hashie::Array, result.class
        assert_equal 1, result.size
      end
    end
    
    def test_get_full_accounts_empty
      assert_raises Peerplays::ArgumentError do
        @api.get_full_accounts()
      end
    end
    
    def test_get_full_accounts_none
      options = [
        [], # string array account_names_or_ids
        false, # boolean array subscribe
      ]
      
      @api.get_full_accounts(*options) do |result|
        assert_equal Hashie::Array, result.class
        assert_equal 0, result.size
      end
    end
    
    def test_get_full_accounts_invalid
      options = [
        ['1234'], # string array account_names_or_ids
        false, # boolean array subscribe
      ]
      
      assert_raises Peerplays::BaseError do
        @api.get_accounts(*options)
      end
    end
    
    def test_get_full_accounts
      options = [
        ['1.2.8900'], # string array account_names_or_ids
        false, # boolean array subscribe
      ]
      
      @api.get_full_accounts(*options) do |result|
        assert_equal Hashie::Array, result.class
        assert_equal 1, result.size
      end
    end
    
    def test_get_account_by_name_empty
      assert_raises Peerplays::ArgumentError do
        @api.get_account_by_name()
      end
    end
    
    def test_get_account_by_name_none
      options = [
        '', # string name
      ]
      
      @api.get_account_by_name(*options) do |result|
        assert_nil result
      end
    end
    
    def test_get_account_by_name_invalid
      options = [
        '1234', # string name
      ]
      
      @api.get_account_by_name(*options) do |result|
        assert_nil result
      end
    end
    
    def test_get_account_by_name
      options = [
        'init1', # string name
      ]
      
      @api.get_account_by_name(*options) do |result|
        assert_equal Hashie::Mash, result.class
        assert_equal 20, result.size, "expect 20 keys, got: #{result.keys}"
      end
    end
    
    def test_get_account_references_empty
      assert_raises Peerplays::ArgumentError do
        @api.get_account_references()
      end
    end
    
    def test_get_account_references_none
      options = [
        '', # string name
      ]
      
      assert_raises Peerplays::ArgumentError do
        @api.get_account_references(*options)
      end
    end
    
    def test_get_account_references_invalid
      options = [
        '1234', # string name
      ]
      
      assert_raises Peerplays::BaseError do
        @api.get_account_references(*options)
      end
    end
    
    def test_get_account_references
      options = [
        'init1', # string name
      ]
      
      @api.get_account_references(*options) do |result|
        assert_equal Hashie::Array, result.class
        assert_equal 0, result.size
      end
    end
    
    def test_lookup_account_names_none
      options = [
        [] # string array account_names
      ]
      
      @api.lookup_account_names(*options) do |result|
        assert_equal Hashie::Array, result.class
        assert_equal 0, result.size
      end
    end
    
    def test_lookup_account_names
      options = [
        ['1.11.6722984'] # string array account_names
      ]
      
      @api.lookup_account_names(*options) do |result|
        assert_equal Hashie::Array, result.class
        assert_equal 1, result.size
      end
    end
    
    def test_lookup_accounts_empty
      assert_raises Peerplays::ArgumentError do
        @api.lookup_accounts()
      end
    end
    
    def test_lookup_accounts_none
      options = [
        '', # string lower_bound_name
        1 # uint32_t limit
      ]
      
      @api.lookup_accounts(*options) do |result|
        assert_equal Hashie::Array, result.class
        assert_equal 1, result.size
      end
    end
    
    def test_lookup_accounts
      options = [
        'a', # string lower_bound_name
        1 # uint32_t limit
      ]
      
      @api.lookup_accounts(*options) do |result|
        assert_equal Hashie::Array, result.class
        assert_equal 1, result.size
      end
    end
    
    def test_get_account_count
      refute_equal 0, @api.get_account_count()
    end
    
    def test_get_account_balances_invalid
      assert_raises Peerplays::ArgumentError do
        @api.get_account_balances('null', [])
      end
    end
    
    def test_get_account_balances
      assert_equal 0, @api.get_account_balances('init1', []).size
    end
    
    def test_get_named_account_balances_invalid
      assert_raises Peerplays::ArgumentError do
        @api.get_named_account_balances('null', [])
      end
    end
    
    def test_get_named_account_balances
      assert_equal 0, @api.get_named_account_balances('init1', []).size
    end
  
    def test_get_balance_objects_empty
      assert_raises Peerplays::ArgumentError do
        @api.get_balance_objects()
      end
    end

    def test_get_vested_balances_empty
      assert_raises Peerplays::ArgumentError do
        @api.get_vested_balances()
      end
    end

    def test_get_vesting_balances_empty
      assert_raises Peerplays::ArgumentError do
        @api.get_vesting_balances()
      end
    end

    def test_get_assets_empty
      assert_raises Peerplays::ArgumentError do
        @api.get_assets()
      end
    end

    def test_list_assets_empty
      assert_raises Peerplays::ArgumentError do
        @api.list_assets()
      end
    end

    def test_lookup_asset_symbols_empty
      assert_raises Peerplays::ArgumentError do
        @api.lookup_asset_symbols()
      end
    end

    def test_get_order_book_empty
      assert_raises Peerplays::ArgumentError do
        @api.get_order_book()
      end
    end

    def test_get_limit_orders_empty
      assert_raises Peerplays::ArgumentError do
        @api.get_limit_orders()
      end
    end

    def test_get_call_orders_empty
      assert_raises Peerplays::ArgumentError do
        @api.get_call_orders()
      end
    end

    def test_get_settle_orders_empty
      assert_raises Peerplays::ArgumentError do
        @api.get_settle_orders()
      end
    end

    def test_get_margin_positions_empty
      assert_raises Peerplays::ArgumentError do
        @api.get_margin_positions()
      end
    end

    def test_subscribe_to_market_empty
      assert_raises Peerplays::ArgumentError do
        @api.subscribe_to_market()
      end
    end

    def test_unsubscribe_from_market_empty
      assert_raises Peerplays::ArgumentError do
        @api.unsubscribe_from_market()
      end
    end

    def test_get_ticker_empty
      assert_raises Peerplays::ArgumentError do
        @api.get_ticker()
      end
    end

    def test_get_24_volume_empty
      assert_raises Peerplays::ArgumentError do
        @api.get_24_volume()
      end
    end

    def test_get_trade_history_empty
      assert_raises Peerplays::ArgumentError do
        @api.get_trade_history()
      end
    end

    def test_get_witnesses_empty
      assert_raises Peerplays::ArgumentError do
        @api.get_witnesses()
      end
    end
    
    def test_get_witness_by_account_empty
      assert_raises Peerplays::ArgumentError do
        @api.get_witness_by_account()
      end
    end
    
    def test_lookup_witness_accounts_empty
      assert_raises Peerplays::ArgumentError do
        @api.lookup_witness_accounts()
      end
    end
    
    def test_get_witness_count
      refute_equal 0, @api.get_witness_count()
    end
    
    def test_get_committee_members_empty
      assert_raises Peerplays::ArgumentError do
        @api.get_committee_members()
      end
    end
    
    def test_get_committee_member_by_account_empty
      assert_raises Peerplays::ArgumentError do
        @api.get_committee_member_by_account()
      end
    end
    
    def test_lookup_committee_member_accounts_empty
      assert_raises Peerplays::ArgumentError do
        @api.lookup_committee_member_accounts()
      end
    end
    
    def test_get_workers_by_account_empty
      assert_raises Peerplays::ArgumentError do
        @api.get_workers_by_account()
      end
    end
    
    def test_lookup_vote_ids_empty
      assert_raises Peerplays::ArgumentError do
        @api.lookup_vote_ids()
      end
    end
    
    def test_get_transaction_hex_empty
      assert_raises Peerplays::ArgumentError do
        @api.get_transaction_hex()
      end
    end
    
    def test_get_transaction_hex
      trx = {
        ref_block_num: 0,
        ref_block_prefix: 0,
        expiration: "1970-01-01T00:00:00",
        operations: [],
        extensions: [],
        signatures: []
      }
      
      @api.get_transaction_hex(trx) do |result|
        assert_equal String, result.class
        assert_equal '00000000000000000000000000', result
      end
    end
    
    def test_get_required_signatures_empty
      assert_raises Peerplays::ArgumentError do
        @api.get_required_signatures()
      end
    end
    
    def test_get_potential_signatures_empty
      assert_raises Peerplays::ArgumentError do
        @api.get_potential_signatures()
      end
    end
    
    def test_get_potential_address_signatures_empty
      assert_raises Peerplays::ArgumentError do
        @api.get_potential_address_signatures()
      end
    end
    
    def test_verify_authority_empty
      assert_raises Peerplays::ArgumentError do
        @api.verify_authority()
      end
    end
    
    def test_verify_account_authority_empty
      assert_raises Peerplays::ArgumentError do
        @api.verify_account_authority()
      end
    end
    
    def test_validate_transaction_empty
      assert_raises Peerplays::ArgumentError do
        @api.validate_transaction()
      end
    end
    
    def test_get_required_fees_empty
      assert_raises Peerplays::ArgumentError do
        @api.get_required_fees()
      end
    end
    
    def test_get_proposed_transactions_empty
      assert_raises Peerplays::ArgumentError do
        @api.get_proposed_transactions()
      end
    end
    
    def test_get_blinded_balances_empty
      assert_raises Peerplays::ArgumentError do
        @api.get_blinded_balances()
      end
    end
  end
end
