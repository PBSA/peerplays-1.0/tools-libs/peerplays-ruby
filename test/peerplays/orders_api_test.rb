require 'test_helper'

module Peerplays
  class OrdersApiTest < Peerplays::Test
    def setup
      @api = Peerplays::OrdersApi.new(url: TEST_NODE, check_enabled: true) rescue skip('not enabled')
    end
    
    def test_method_missing
      assert_raises NoMethodError do
        @api.bogus
      end
    end
    
    def test_get_grouped_limit_orders_empty
      assert_raises Peerplays::ArgumentError do
        @api.get_grouped_limit_orders()
      end
    end
  end
end
