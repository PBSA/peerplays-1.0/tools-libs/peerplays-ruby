require 'test_helper'

module Peerplays
  class BaseErrorTest < Peerplays::Test
    def test_method_missing
      assert_raises BaseError do
        raise BaseError
      end
    end
    
    def test_to_s
      assert_equal '{}', BaseError.new.to_s
    end
  end
end
