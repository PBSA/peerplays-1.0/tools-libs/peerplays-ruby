require 'test_helper'

module Peerplays
  class CryptoApiTest < Peerplays::Test
    def setup
      @api = Peerplays::CryptoApi.new(url: TEST_NODE, check_enabled: true) rescue skip('not enabled')
    end
    
    def test_method_missing
      assert_raises NoMethodError do
        @api.bogus
      end
    end
    
    def test_blind_empty
      assert_raises Peerplays::ArgumentError do
        @api.blind()
      end
    end
    
    def test_blind
      options = [
        '', # blind_factor_type blind
        1 # uint64_t value
      ]
      
      @api.blind(*options) do |result|
        assert_equal String, result.class
        assert_equal '0250929b74c1a04954b78b4b6035e97a5e078a5a0f28ec96d547bfee9ace803ac0', result
      end
    end
    
    def test_blind_sum_empty
      assert_raises Peerplays::ArgumentError do
        @api.blind_sum()
      end
    end
    
    def test_blind_sum
      options = [
        [''], # blind_factor_type array blinds_in
        1 # uint64_t non_neg
      ]
      
      @api.blind_sum(*options) do |result|
        assert_equal String, result.class
        assert_equal '0000000000000000000000000000000000000000000000000000000000000000', result
      end
    end
    
    def test_range_get_info_empty
      assert_raises Peerplays::ArgumentError do
        @api.range_get_info()
      end
    end
    
    def test_range_proof_sign_empty
      assert_raises Peerplays::ArgumentError do
        @api.range_proof_sign()
      end
    end
    
    def test_verify_sum_empty
      assert_raises Peerplays::ArgumentError do
        @api.verify_sum()
      end
    end
    
    def test_verify_range_empty
      assert_raises Peerplays::ArgumentError do
        @api.verify_range()
      end
    end
    
    def test_verify_range_proof_rewind_empty
      assert_raises Peerplays::ArgumentError do
        @api.verify_range_proof_rewind()
      end
    end
  end
end
