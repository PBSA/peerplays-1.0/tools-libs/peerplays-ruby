require 'test_helper'

module Peerplays
  class HistoryApiTest < Peerplays::Test
    def setup
      @api = Peerplays::HistoryApi.new(url: TEST_NODE, check_enabled: true) rescue skip('not enabled')
    end
    
    def test_method_missing
      assert_raises NoMethodError do
        @api.bogus
      end
    end
    
    def test_get_account_history_empty
      assert_raises Peerplays::ArgumentError do
        @api.get_account_history()
      end
    end
    
    def test_get_account_history
      options = [
        '1.2.8900', # string account_id_or_name
        '1.11.6722984', # operation_history_id_type stop
        1, # unsigned limit
        '1.11.8140655', # operation_history_id_type start
      ]
      
      @api.get_account_history(*options) do |result|
        assert_equal Hashie::Array, result.class
        assert_equal 1, result.size
      end
    end
    
    def test_get_account_history_operations_empty
      assert_raises Peerplays::ArgumentError do
        @api.get_account_history_operations()
      end
    end
    
    def test_get_account_history_operations
      options = [
        '1.2.8900', # string account_id_or_name
        49, # int operation_type
        '1.11.8140655', # operation_history_id_type start
        '1.11.6722984', # operation_history_id_type stop
        1, # unsigned limit
      ]
      
      @api.get_account_history_operations(*options) do |result|
        assert_equal Hashie::Array, result.class
        assert_equal 1, result.size
      end
    end
    
    def test_get_relative_account_history_empty
      assert_raises Peerplays::ArgumentError do
        @api.get_relative_account_history()
      end
    end
    
    def test_get_relative_account_history
      options = [
        '1.2.8900', # string account_id_or_name
        0, # uint64_t stop
        2, # unsigned limit
        0 # uint64_t start
      ]
      
      @api.get_relative_account_history(*options) do |result|
        assert_equal Hashie::Array, result.class
        assert_equal 2, result.size
      end
    end
    
    def test_get_fill_order_history_empty
      assert_raises Peerplays::ArgumentError do
        @api.get_fill_order_history()
      end
    end
    
    def test_get_fill_order_history_none
      options = [
        'PPY', # string a
        'PPY', # string b
        0, # unsigned limit
      ]
      
      @api.get_fill_order_history(*options) do |result|
        assert_equal Hashie::Array, result.class
        assert_equal 0, result.size
      end
    end
    
    
    def test_get_fill_order_history
      options = [
        'PPY', # string a
        'BTFUN', # string b
        1, # unsigned limit
      ]
      
      @api.get_fill_order_history(*options) do |result|
        assert_equal Hashie::Array, result.class
        assert_equal 0, result.size
      end
    end
    
    def test_get_market_history_empty
      assert_raises Peerplays::ArgumentError do
        @api.get_market_history()
      end
    end
    
    def test_get_market_history_buckets
      @api.get_market_history_buckets do |result|
        assert_equal Hashie::Array, result.class
        assert_equal 5, result.size
      end
    end
  end
end
