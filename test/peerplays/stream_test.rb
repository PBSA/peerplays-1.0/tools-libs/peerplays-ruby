require 'test_helper'

module Peerplays
  class StreamTest < Peerplays::Test
    def setup
      @stream = Peerplays::Stream.new(url: TEST_NODE, check_enabled: true) rescue skip('not enabled')
      database_api = Peerplays::DatabaseApi.new(url: TEST_NODE, check_enabled: true) rescue skip('not enabled')
      
      database_api.get_dynamic_global_properties do |properties|
        @head_block_num = properties.head_block_number
        @last_irreversible_block_num = properties.last_irreversible_block_num
      end
    end
    
    def test_block_numbers
      options = {
        until_block_num: @last_irreversible_block_num + 1
      }
    
      @stream.block_numbers(options) do |block_num|
        assert block_num
      end
    end
    
    def test_block_headers
      options = {
        until_block_num: @last_irreversible_block_num + 1
      }
    
      @stream.block_headers(options) do |block_header, block_num|
        assert block_header
        assert block_num
      end
    end
    
    def test_block_headers_mode_head
      stream = Peerplays::Stream.new(url: TEST_NODE, mode: :head)
      options = {
        until_block_num: @head_block_num + 1
      }
      
      stream.block_headers(options) do |block_header, block_num|
        assert block_header
        assert block_num
      end
    end
    
    def test_block_headers_mode_bogus
      stream = Peerplays::Stream.new(url: TEST_NODE, mode: :WRONG)
      options = {
        until_block_num: @head_block_num + 1
      }
      
      assert_raises Peerplays::ArgumentError do
        stream.block_headers(options) do |block_header, block_num|
          fail 'should be unreachable'
        end
      end
    end
    
    def test_blocks
      options = {
        until_block_num: @last_irreversible_block_num + 1
      }
      
      @stream.blocks(options) do |block, block_num|
        assert block
        assert block_num
        assert block_num >= @last_irreversible_block_num, "expect block_num: #{block_num} greater than or equal to last_irreversible_block_num: #{@last_irreversible_block_num}"
      end
    end
    
    def test_blocks_by_range
      range = @last_irreversible_block_num..(@last_irreversible_block_num + 1)
      options = {
        block_range: range
      }
      
      @stream.blocks(options) do |block, block_num|
        assert block
        assert block_num
      end
    end
    
    def test_transactions
      options = {
        # Use this if the blockchain active (without at_block_num).
        # until_block_num: @last_irreversible_block_num + 1
        at_block_num: 11000,
        until_block_num: 11010
      }
      
      @stream.transactions(options) do |trx, block_num|
        assert trx
        assert block_num
      end
    end
    
    def test_operations
      options = {
        # Use this if the blockchain active (without at_block_num).
        # until_block_num: @last_irreversible_block_num + 1
        at_block_num: 11000,
        until_block_num: 11010
      }
      
      @stream.operations(options) do |op_type, op_value, block_num|
        assert op_type
        assert op_value
        assert block_num
      end
    end
    
    def test_operations_by_type
      options = {
        # Use this if the blockchain active (without at_block_num).
        # until_block_num: @last_irreversible_block_num + 1
        at_block_num: 14451,
        until_block_num: 14452,
        types: :transfer
      }
      
      @stream.operations(options) do |op_type, op_value, block_num|
        assert op_type
        assert op_value
        assert block_num
      end
    end
    
    def test_operations_by_type_args
      skip 'cannot test this because we cannot express until_block_num'
      
      @stream.operations(:account_update, :transfer) do |op_type, op_value, block_num|
        assert op_type
        assert op_value
        assert block_num
      end
    end
    
    def test_operations_by_full_type
      account_update = false
      options = {
        # Use this if the blockchain active (without at_block_num).
        # until_block_num: @last_irreversible_block_num + 1
        at_block_num: 16921,
        until_block_num: 16922,
        types: :account_create_operation
      }
      
      @stream.operations(options) do |op_type, op_value, block_num|
        assert op_type
        assert op_value
        assert block_num
        
        account_update = true
      end
        
      fail 'no account_create ops found' unless account_update
    end
  end
end
