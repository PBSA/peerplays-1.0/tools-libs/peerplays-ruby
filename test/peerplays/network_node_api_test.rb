require 'test_helper'

module Peerplays
  class NetworkNodeApiTest < Peerplays::Test
    def setup
      @api = Peerplays::NetworkNodeApi.new(url: TEST_NODE, check_enabled: true) rescue skip('not enabled')
    end
    
    def test_method_missing
      assert_raises NoMethodError do
        @api.bogus
      end
    end
    
    def test_get_info_empty
      assert_raises Peerplays::ArgumentError do
        @api.get_info()
      end
    end
    
    def test_get_connected_peers_empty
      assert_raises Peerplays::ArgumentError do
        @api.get_connected_peers()
      end
    end
    
    def test_get_potential_peers_empty
      assert_raises Peerplays::ArgumentError do
        @api.get_potential_peers()
      end
    end
    
    def test_get_advanced_node_parameters_empty
      assert_raises Peerplays::ArgumentError do
        @api.get_advanced_node_parameters()
      end
    end
    
    def test_add_node_empty
      assert_raises Peerplays::ArgumentError do
        @api.add_node()
      end
    end
    
    def test_set_advanced_node_parameters_empty
      assert_raises Peerplays::ArgumentError do
        @api.set_advanced_node_parameters()
      end
    end
  end
end
