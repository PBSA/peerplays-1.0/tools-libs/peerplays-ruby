$LOAD_PATH.unshift File.expand_path('../../lib', __FILE__)

require 'simplecov'

SimpleCov.start
SimpleCov.merge_timeout 3600

require 'peerplays'
require 'minitest/autorun'
require 'minitest/line/describe_track'
require 'yaml'
require 'awesome_print'
require 'minitest/hell'
require 'minitest/proveit'

class Minitest::Test
  # See: https://gist.github.com/chrisroos/b5da6c6a37ac8af5fe78
  parallelize_me! unless defined? WebMock
end

class Peerplays::Test < MiniTest::Test
  defined? prove_it! and prove_it!
  
  TEST_NODE = ENV.fetch 'TEST_NODE', Peerplays::ChainConfig::NETWORKS_PEERPLAYS_DEFAULT_NODE
end
