# encoding: UTF-8
require 'json' unless defined?(JSON)
require 'net/https'

require 'hashie'
require 'peerplays/version'
require 'peerplays/chain_config'
require 'peerplays/base_error'
require 'peerplays/api'
require 'peerplays/history_api'
require 'peerplays/asset_api'
require 'peerplays/block_api'
require 'peerplays/crypto_api'
require 'peerplays/database_api'
require 'peerplays/network_broadcast_api'
require 'peerplays/network_node_api'
require 'peerplays/orders_api'
require 'peerplays/wallet_api'
require 'peerplays/stream'
require 'peerplays/operation'

module Peerplays
end

Hashie.logger = Logger.new(ENV['HASHIE_LOGGER'])
