module Peerplays
  class BaseError < StandardError
    def initialize(error = nil, cause = nil)
      @error = error
      @cause = cause
    end
    
    def to_s
      detail = {}
      detail[:error] = @error if !!@error
      detail[:cause] = @cause if !!@cause
      
      JSON[detail] rescue detail.to_s
    end
    
    def self.build_error(error, context)
      if error.message =~ /too few arguments passed to method/
        raise ArgumentError, "#{error.message}: #{context}:", build_backtrace(error)
      elsif error.message =~ /Invalid cast/
        raise ArgumentError, "#{error.message}: #{context}:", build_backtrace(error)
      elsif error.message =~ /doesn't match expected value/
        raise ArgumentError, "#{error.message}: #{context}:", build_backtrace(error)
      elsif error.message =~ /name_or_id.size\(\) > 0/
        raise ArgumentError, "#{error.message}: #{context}:", build_backtrace(error)
      elsif error.message =~ /account: no such account/
        raise ArgumentError, "#{error.message}: #{context}:", build_backtrace(error)
      elsif error.message =~ /A transaction must have at least one operation/
        raise EmptyTransactionError, "#{error.message}: #{context}:", build_backtrace(error)
      elsif error.message =~ /attempting to push a block that is too old/
        raise BlockTooOldError, "#{error.message}: #{context}:", build_backtrace(error)
      else
        raise UnknownError, "#{error.message}: #{context}:", build_backtrace(error)
      end
    end
  private
    def self.build_backtrace(error)
      backtrace = Thread.current.backtrace.reject{ |line| line =~ /base_error/ }
      JSON.pretty_generate(error) + "\n" + backtrace.join("\n")
    end
  end
  
  class ArgumentError < BaseError; end
  class RemoteNodeError < BaseError; end
  class UpstreamResponseError < RemoteNodeError; end
  class RequestTimeoutUpstreamResponseError < UpstreamResponseError; end
  class EmptyTransactionError < ArgumentError; end
  class BlockTooOldError < BaseError; end
  class UnknownError < BaseError; end
end
