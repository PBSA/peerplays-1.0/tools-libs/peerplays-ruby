module Peerplays
  # See: {https://www.peerplays.tech/api/peerplays-core-api/account-history-api History API}
  class HistoryApi < Peerplays::Api
    HISTORY_API_NAMESPACE = 'history'.freeze
    HISTORY_API_METHODS = %i(
      get_account_history
      get_account_history_operations
      get_relative_account_history
      get_fill_order_history
      get_market_history
      get_market_history_buckets
    ).freeze
      
    def initialize(options = {})
      @api_name = HISTORY_API_NAMESPACE
      @methods = HISTORY_API_METHODS
      
      super
    end
  end
end
