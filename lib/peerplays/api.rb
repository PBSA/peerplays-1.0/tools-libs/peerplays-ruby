require 'faye/websocket'
require 'eventmachine'

module Peerplays
  # See: {https://www.peerplays.tech/api/peerplays-core-api Core API}
  class Api
    attr_reader :methods
    
    INITIAL_LATCH = 0.01
    MAX_LATCH = 3.0
    
    def initialize(options = {})
      @url = options[:url] || ChainConfig::NETWORKS_PEERPLAYS_DEFAULT_NODE
      @rpc_id = 0
      @latch = INITIAL_LATCH
      @socket = nil
      @messages = nil
      @thread_running = false
      @api_id = api_id if !!@api_name && !!options[:check_enabled]
      @api_id ||= options[:api_id] || @api_name
    end
    
    def api_id
      @api_id = socket_send(1, @api_name, [])
    end
    
    def socket_send(*args)
      id = rpc_id
      send_options = {id: id, method: :call, params: args}
      
      socket.send(send_options.to_json)
      latch_reset
      latch until !!(message = messages.delete(id))
      
      if !!message['error']
        BaseError.build_error(message['error'], send_options)
      end
      
      message['result']
    end
    
    def socket
      start_thread unless !!@thread_running
      
      @socket
    end
    
    def messages
      start_thread unless !!@thread_running
      
      @messages
    end
  private
    # @private
    def start_thread
      Thread.new do
        EM.run {
          @socket ||= Faye::WebSocket::Client.new(@url)
          
          @socket.on :open do |event|
            @socket.send({id: rpc_id, method: 'call', params: [1, 'login', ['','']]}.to_json)
            @thread_running = true
          end
          
          @socket.on :message do |event|
            @messages ||= {}
            
            JSON[event.data].tap do |object|
              @messages[object['id']] = Hashie::Mash.new(object.select{|k| !%w(id jsonrpc).include? k})
            end
          end
          
          @socket.on :close do |event|
            @thread_running = false
            @socket = nil
          end
        }
      end
      
      latch_reset
      latch until !!@socket && !!@messages && @thread_running
    end
    
    # @private
    def respond_to_missing?(m, include_private = false)
      @methods.nil? ? false : @methods.include?(m.to_sym)
    end
    
    # @private
    def method_missing(m, *args, &block)
      super unless respond_to_missing?(m)
      
      return socket_send(@api_id, m, args) unless block_given?
      
      yield socket_send(@api_id, m, args)
    end
    
    # @private
    def latch_reset
      @latch = INITIAL_LATCH
    end
    
    # @private
    def latch
      raise RequestTimeoutUpstreamResponseError if @latch > MAX_LATCH * 2
      
      sleep [@latch *= 2, MAX_LATCH].min
    end
    
    # @private
    def rpc_id
      @rpc_id = @rpc_id + 1
    end
  end
end
