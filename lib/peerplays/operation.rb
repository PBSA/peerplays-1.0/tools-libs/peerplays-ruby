module Peerplays
  class Operation
    # IDs derrived from:
    # https://gitlab.com/PBSA/peerplays/-/blob/master/libraries/chain/include/graphene/chain/protocol/operations.hpp#L57-138
    
    IDS = [
      :transfer_operation,
      :limit_order_create_operation,
      :limit_order_cancel_operation,
      :call_order_update_operation,
      :fill_order_operation, # VIRTUAL
      :account_create_operation,
      :account_update_operation,
      :account_whitelist_operation,
      :account_upgrade_operation,
      :account_transfer_operation,
      :asset_create_operation,
      :asset_update_operation,
      :asset_update_bitasset_operation,
      :asset_update_feed_producers_operation,
      :asset_issue_operation,
      :asset_reserve_operation,
      :asset_fund_fee_pool_operation,
      :asset_settle_operation,
      :asset_global_settle_operation,
      :asset_publish_feed_operation,
      :witness_create_operation,
      :witness_update_operation,
      :proposal_create_operation,
      :proposal_update_operation,
      :proposal_delete_operation,
      :withdraw_permission_create_operation,
      :withdraw_permission_update_operation,
      :withdraw_permission_claim_operation,
      :withdraw_permission_delete_operation,
      :committee_member_create_operation,
      :committee_member_update_operation,
      :committee_member_update_global_parameters_operation,
      :vesting_balance_create_operation,
      :vesting_balance_withdraw_operation,
      :worker_create_operation,
      :custom_operation,
      :assert_operation,
      :balance_claim_operation,
      :override_transfer_operation,
      :transfer_to_blind_operation,
      :blind_transfer_operation,
      :transfer_from_blind_operation,
      :asset_settle_cancel_operation, # VIRTUAL
      :asset_claim_fees_operation,
      :fba_distribute_operation, # VIRTUAL
      :tournament_create_operation,
      :tournament_join_operation,
      :game_move_operation,
      :asset_update_dividend_operation,
      :asset_dividend_distribution_operation, # VIRTUAL
      :tournament_payout_operation, # VIRTUAL
      :tournament_leave_operation,
      :sport_create_operation,
      :sport_update_operation,
      :event_group_create_operation,
      :event_group_update_operation,
      :event_create_operation,
      :event_update_operation,
      :betting_market_rules_create_operation,
      :betting_market_rules_update_operation,
      :betting_market_group_create_operation,
      :betting_market_create_operation,
      :bet_place_operation,
      :betting_market_group_resolve_operation,
      :betting_market_group_resolved_operation, # VIRTUAL
      :bet_adjusted_operation, # VIRTUAL
      :betting_market_group_cancel_unmatched_bets_operation,
      :bet_matched_operation, # VIRTUAL
      :bet_cancel_operation,
      :bet_canceled_operation, # VIRTUAL
      :betting_market_group_update_operation,
      :betting_market_update_operation,
      :event_update_status_operation,
      :sport_delete_operation,
      :event_group_delete_operation,
      :affiliate_payout_operation, # VIRTUAL
      :affiliate_referral_payout_operation, # VIRTUAL
      :lottery_asset_create_operation,
      :ticket_purchase_operation,
      :lottery_reward_operation,
      :lottery_end_operation,
      :sweeps_vesting_claim_operation
    ]
    
    def self.op_id(op)
      IDS.find_index op
    end
  end
end
