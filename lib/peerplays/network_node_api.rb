module Peerplays
  # See: {https://www.peerplays.tech/api/peerplays-core-api/network-nodes-api Network Node API}
  class NetworkNodeApi < Peerplays::Api
    NETWORK_NODE_API_NAMESPACE = 'network_node'.freeze
    NETWORK_NODE_API_METHODS = %i(
      get_info
      get_connected_peers
      get_potential_peers
      get_advanced_node_parameters
      add_node
      set_advanced_node_parameters
    ).freeze
    
    def initialize(options = {})
      @api_name = NETWORK_NODE_API_NAMESPACE
      @methods = NETWORK_NODE_API_METHODS
      
      super
    end
  end
end
