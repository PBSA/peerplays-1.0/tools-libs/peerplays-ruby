module Peerplays
  # See: {https://www.peerplays.tech/api/peerplays-core-api/orders-api Orders API}
  class OrdersApi < Peerplays::Api
    ORDERS_API_NAMESPACE = 'orders'.freeze
    ORDERS_API_METHODS = %i(
      get_grouped_limit_orders
    ).freeze
    
    def initialize(options = {})
      @api_name = ORDERS_API_NAMESPACE
      @methods = ORDERS_API_METHODS
      
      super
    end
  end
end
