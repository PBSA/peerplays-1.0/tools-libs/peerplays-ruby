module Peerplays
  # Peerplays::Stream allows a live view of the Peerplays blockchain.
  # 
  # Example streaming blocks:
  # 
  #     stream = Peerplays::Stream.new
  #     
  #     stream.blocks do |block, block_num|
  #       puts "#{block_num} :: #{block.witness}"
  #     end
  # 
  # Example streaming transactions:
  # 
  #     stream = Peerplays::Stream.new
  #     
  #     stream.transactions do |trx, block_num|
  #       puts "#{block_num} :: operations: #{trx.operations.size}"
  #     end
  # 
  # Example streaming operations:
  # 
  #     stream = Peerplays::Stream.new
  #    
  #     stream.operations do |op, block_num|
  #       puts "#{block_num} :: #{op.type}: #{op.value.to_json}"
  #     end
  # 
  # Allows streaming of block headers, full blocks, transactions, operations.
  class Stream
    attr_reader :mode
    
    BLOCK_INTERVAL = 3
    MAX_BACKOFF_BLOCK_INTERVAL = 30
    MAX_RETRY_COUNT = 10
    
    # @param options [Hash] additional options
    # @option options [Symbol] :mode we have the choice between
    #   * :head the last block
    #   * :irreversible the block that is confirmed by 2/3 of all block producers and is thus irreversible!
    def initialize(options = {mode: :irreversible})
      @instance_options = options
      @mode = options[:mode] || :irreversible
    end
    
    # Use this method to stream block numbers.  This is significantly faster
    # than requesting full blocks and even block headers.  Basically, the only
    # thing this method does is call {Peerplays::Database#get_dynamic_global_properties} at 3 second
    # intervals.
    #
    # @param options [Hash] additional options
    # @option options [Integer] :at_block_num Starts the stream at the given block number.  Default: nil.
    # @option options [Integer] :until_block_num Ends the stream at the given block number.  Default: nil.
    def block_numbers(options = {}, &block)
      block_objects(options.merge(object: :block_numbers), block)
    end
    
    # Use this method to stream block headers.  This is quite a bit faster than
    # requesting full blocks.
    #
    # @param options [Hash] additional options
    # @option options [Integer] :at_block_num Starts the stream at the given block number.  Default: nil.
    # @option options [Integer] :until_block_num Ends the stream at the given block number.  Default: nil.
    def block_headers(options = {}, &block)
      block_objects(options.merge(object: :block_header), block)
    end
    
    # Use this method to stream full blocks.
    #
    # @param options [Hash] additional options
    # @option options [Integer] :at_block_num Starts the stream at the given block number.  Default: nil.
    # @option options [Integer] :until_block_num Ends the stream at the given block number.  Default: nil.
    def blocks(options = {}, &block)
      block_objects(options.merge(object: :block), block)
    end
    
    # Use this method to stream each transaction.
    #
    # @param options [Hash] additional options
    # @option options [Integer] :at_block_num Starts the stream at the given block number.  Default: nil.
    # @option options [Integer] :until_block_num Ends the stream at the given block number.  Default: nil.
    def transactions(options = {}, &block)
      blocks(options) do |block, block_num|
        block.transactions.each_with_index do |transaction, index|
          yield transaction, block_num
        end
      end
    end
    
    # Returns the latest operations from the blockchain.
    #
    #   stream = Peerplays::Stream.new
    #   stream.operations do |op|
    #     puts op.to_json
    #   end
    # 
    # If symbol are passed to `types` option, then only that operation is
    # returned.  Expected symbols are:
    #
    #   :transfer_operation
    #   :limit_order_create_operation
    #   :limit_order_cancel_operation
    #   :call_order_update_operation
    #   :fill_order_operation # VIRTUAL
    #   :account_create_operation
    #   :account_update_operation
    #   :account_whitelist_operation
    #   :account_upgrade_operation
    #   :account_transfer_operation
    #   :asset_create_operation
    #   :asset_update_operation
    #   :asset_update_bitasset_operation
    #   :asset_update_feed_producers_operation
    #   :asset_issue_operation
    #   :asset_reserve_operation
    #   :asset_fund_fee_pool_operation
    #   :asset_settle_operation
    #   :asset_global_settle_operation
    #   :asset_publish_feed_operation
    #   :witness_create_operation
    #   :witness_update_operation
    #   :proposal_create_operation
    #   :proposal_update_operation
    #   :proposal_delete_operation
    #   :withdraw_permission_create_operation
    #   :withdraw_permission_update_operation
    #   :withdraw_permission_claim_operation
    #   :withdraw_permission_delete_operation
    #   :committee_member_create_operation
    #   :committee_member_update_operation
    #   :committee_member_update_global_parameters_operation
    #   :vesting_balance_create_operation
    #   :vesting_balance_withdraw_operation
    #   :worker_create_operation
    #   :custom_operation
    #   :assert_operation
    #   :balance_claim_operation
    #   :override_transfer_operation
    #   :transfer_to_blind_operation
    #   :blind_transfer_operation
    #   :transfer_from_blind_operation
    #   :asset_settle_cancel_operation # VIRTUAL
    #   :asset_claim_fees_operation
    #   :fba_distribute_operation # VIRTUAL
    #   :tournament_create_operation
    #   :tournament_join_operation
    #   :game_move_operation
    #   :asset_update_dividend_operation
    #   :asset_dividend_distribution_operation # VIRTUAL
    #   :tournament_payout_operation # VIRTUAL
    #   :tournament_leave_operation
    #   :sport_create_operation
    #   :sport_update_operation
    #   :event_group_create_operation
    #   :event_group_update_operation
    #   :event_create_operation
    #   :event_update_operation
    #   :betting_market_rules_create_operation
    #   :betting_market_rules_update_operation
    #   :betting_market_group_create_operation
    #   :betting_market_create_operation
    #   :bet_place_operation
    #   :betting_market_group_resolve_operation
    #   :betting_market_group_resolved_operation # VIRTUAL
    #   :bet_adjusted_operation # VIRTUAL
    #   :betting_market_group_cancel_unmatched_bets_operation
    #   :bet_matched_operation # VIRTUAL
    #   :bet_cancel_operation
    #   :bet_canceled_operation # VIRTUAL
    #   :betting_market_group_update_operation
    #   :betting_market_update_operation
    #   :event_update_status_operation
    #   :sport_delete_operation
    #   :event_group_delete_operation
    #   :affiliate_payout_operation # VIRTUAL
    #   :affiliate_referral_payout_operation # VIRTUAL
    #   :lottery_asset_create_operation
    #   :ticket_purchase_operation
    #   :lottery_reward_operation
    #   :lottery_end_operation
    #   :sweeps_vesting_claim_operation
    #
    # For example, to stream only transfers:
    #
    #   stream = Peerplays::Stream.new
    #   stream.operations(types: :transfer) do |_, transfer|
    #     puts transfer.to_json
    #   end
    # 
    # ... Or ...
    # 
    #   stream = Peerplays::Stream.new
    #   stream.operations(:transfer) do |_, transfer|
    #     puts transfer.to_json
    #   end
    #
    # @param args [Symbol || Array<Symbol> || Hash] the type(s) of operation or hash of expanded options, optional.
    # @option args [Integer] :at_block_num Starts the stream at the given block number.  Default: nil.
    # @option args [Integer] :until_block_num Ends the stream at the given block number.  Default: nil.
    # @option args [Symbol || Array<Symbol>] :types the type(s) of operation, optional.
    # @param block the block to execute for each result.  Yields: |type_type, op_value, block_num|
    def operations(*args, &block)
      options = {}
      types = []
      last_block_num = nil
      
      case args.first
      when Hash
        options = args.first
        types = transform_types(options[:types])
      when Symbol, Array then types = transform_types(args)
      end
      
      transactions(options) do |transaction, block_num|
        transaction.operations.each do |type, value|
          type = Operation::IDS[type]
          
          yield type, value, block_num if types.none? || types.include?(type)
          
          next unless last_block_num != block_num
          
          last_block_num = block_num
        end
      end
    end
  private
    # @private
    def block_objects(options = {}, block)
      object = options[:object]
      object_method = "get_#{object}".to_sym
      block_interval = BLOCK_INTERVAL
      
      at_block_num, until_block_num = if !!block_range = options[:block_range]
        [block_range.first, block_range.last]
      else
        [options[:at_block_num], options[:until_block_num]]
      end
      
      loop do
        break if !!until_block_num && !!at_block_num && until_block_num < at_block_num
        
        database_api.get_dynamic_global_properties do |properties|
          current_block_num = find_block_number(properties)
          current_block_num = [current_block_num, until_block_num].compact.min
          at_block_num ||= current_block_num
          
          if current_block_num >= at_block_num
            range = at_block_num..current_block_num
            
            if object == :block_numbers
              range.each do |n|
                block.call n
                block_interval = BLOCK_INTERVAL
              end
            else
              range.each do |block_num|
                database_api.send(object_method, block_num) do |b|
                  block.call b, block_num
                  block_interval = BLOCK_INTERVAL
                end
              end
            end
            
            at_block_num = range.max + 1
          else
            # The stream has stalled, so let's back off and let the node sync
            # up.  We'll catch up with a bigger batch in the next cycle.
            block_interval = [block_interval * 2, MAX_BACKOFF_BLOCK_INTERVAL].min
          end
        end
        
        sleep block_interval
      end
    end
    
    # @private
    def find_block_number(properties)
      block_num = case mode.to_sym
      when :head then properties.head_block_number
      when :irreversible then properties.last_irreversible_block_num
      else; raise ArgumentError, "Unknown mode: #{mode}"
      end
      
      block_num
    end
    
    # @private
    def transform_types(types)
      [types].compact.flatten.map do |type|
        type = type.to_s
        
        unless type.end_with? '_operation'
          type += '_operation'
        end
        
        type.to_sym
      end
    end
    
    # @private
    def database_api
      @database_api ||= DatabaseApi.new(@instance_options)
    end
  end
end
