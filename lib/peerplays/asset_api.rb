module Peerplays
  # See: {https://www.peerplays.tech/api/peerplays-core-api/asset-api Asset API}
  class AssetApi < Peerplays::Api
    ASSET_API_NAMESPACE = 'asset'.freeze
    ASSET_API_METHODS = %i(
      get_asset_holders
      get_all_asset_holders
    ).freeze
    
    def initialize(options = {})
      @api_name = ASSET_API_NAMESPACE
      @methods = ASSET_API_METHODS
      
      super
    end
  end
end
