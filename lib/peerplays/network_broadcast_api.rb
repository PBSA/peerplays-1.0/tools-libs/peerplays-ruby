module Peerplays
  # See: {https://www.peerplays.tech/api/peerplays-core-api/network-broadcast-api Network Broadcast API}
  class NetworkBroadcastApi < Peerplays::Api
    NETWORK_BROADCAST_API_NAMESPACE = 'network_broadcast'.freeze
    NETWORK_BROADCAST_API_METHODS = %i(
      broadcast_transaction
      broadcast_transaction_with_callback
      broadcast_block
    ).freeze
    
    def initialize(options = {})
      @api_name = NETWORK_BROADCAST_API_NAMESPACE
      @methods = NETWORK_BROADCAST_API_METHODS
      
      super
    end
  end
end
