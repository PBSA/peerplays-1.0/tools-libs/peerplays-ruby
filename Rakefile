require 'bundler/gem_tasks'
require 'rake/testtask'
require 'yard'
require 'peerplays'
require 'time'

Rake::TestTask.new(:test) do |t|
  t.libs << 'test'
  t.libs << 'lib'
  t.test_files = FileList['test/**/*_test.rb']
  t.ruby_opts << if ENV['HELL_ENABLED']
    '-W2'
  else
    '-W1'
  end
end

YARD::Rake::YardocTask.new do |t|
  t.files = ['lib/**/*.rb']
end

task default: :test

desc 'Ruby console with peerplays already required.'
task :console do
  exec 'irb -r peerplays -r awesome_print -I ./lib'
end

namespace :stream do
  desc 'Test the ability to stream a block range.'
  task :block_range, [:mode, :at_block_num] do |t, args|
    mode = (args[:mode] || 'irreversible').to_sym
    first_block_num = args[:at_block_num].to_i if !!args[:at_block_num]
    stream = Peerplays::Stream.new(url: ENV['TEST_NODE'], mode: mode)
    api = Peerplays::DatabaseApi.new(url: ENV['TEST_NODE'])
    last_block_num = nil
    last_timestamp = nil
    range_complete = false
    
    api.get_dynamic_global_properties do |properties|
      current_block_num = if mode == :head
        properties.head_block_number
      else
        properties.last_irreversible_block_num
      end
      
      # First pass replays latest a random number of blocks to test chunking.
      first_block_num ||= current_block_num - (rand * 200).to_i
      
      range = first_block_num..current_block_num
      puts "Initial block range: #{range.size}"
      
      stream.blocks(at_block_num: range.first) do |block, block_num|
        current_timestamp = Time.parse(block.timestamp + 'Z')
        
        if !range_complete && block_num > range.last
          puts 'Done with initial range.'
          range_complete = true
        end
        
        if !!last_timestamp && block_num != last_block_num + 1
          puts "Bug: Last block number was #{last_block_num} then jumped to: #{block_num}"
          exit
        end
        
        if !!last_timestamp && current_timestamp < last_timestamp
          puts "Bug: Went back in time.  Last timestamp was #{last_timestamp}, then jumped back to #{current_timestamp}"
          exit
        end
        
        puts "\t#{block_num} Timestamp: #{current_timestamp}, witness: #{block.witness}"
        last_block_num = block_num
        last_timestamp = current_timestamp
      end
    end
  end
  
  desc 'Test the ability to stream a block range of transactions.'
  task :trx_range, [:mode, :at_block_num] do |t, args|
    mode = (args[:mode] || 'irreversible').to_sym
    first_block_num = args[:at_block_num].to_i if !!args[:at_block_num]
    stream = Peerplays::Stream.new(url: ENV['TEST_NODE'], mode: mode)
    api = Peerplays::DatabaseApi.new(url: ENV['TEST_NODE'])
    
    api.get_dynamic_global_properties do |properties|
      current_block_num = if mode == :head
        properties.head_block_number
      else
        properties.last_irreversible_block_num
      end
      
      # First pass replays latest a random number of blocks to test chunking.
      first_block_num ||= current_block_num - (rand * 200).to_i
      
      stream.transactions(at_block_num: first_block_num) do |trx, block_num|
        puts "#{block_num} :: ops: #{trx.operations.map{|op| Peerplays::Operation::IDS[op[0]]}.join(', ')}"
      end
    end
  end
  
  desc 'Test the ability to stream a block range of operations.'
  task :op_range, [:mode, :at_block_num] do |t, args|
    mode = (args[:mode] || 'irreversible').to_sym
    first_block_num = args[:at_block_num].to_i if !!args[:at_block_num]
    stream = Peerplays::Stream.new(url: ENV['TEST_NODE'], mode: mode)
    api = Peerplays::DatabaseApi.new(url: ENV['TEST_NODE'])
    
    api.get_dynamic_global_properties do |properties|
      current_block_num = if mode == :head
        properties.head_block_number
      else
        properties.last_irreversible_block_num
      end
      
      # First pass replays latest a random number of blocks to test chunking.
      first_block_num ||= current_block_num - (rand * 200).to_i
      
      stream.operations(at_block_num: first_block_num) do |op_type, op_value, block_num|
        puts "#{block_num} :: op: #{op_type}"
      end
    end
  end
end
